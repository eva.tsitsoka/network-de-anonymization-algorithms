#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include "utilities.h"


/* 
 * Assistive functions needed mainly for testing purposes.
 * The noise models used for our experiments are defined here.
 */

/*
 * TODO : Maybe add a function to free graph variables.
 * TODO : Maybe change read_snap to be consistent with the two other functions.
 */

/*
 * Implementation of Durstenfeld version of Fisher-Yates shuffle algorithm
 *
 * The function assumes that the caller has already allocated memory for variable randperm.
 *
 * References
 * ------------
 * [1] http://en.wikipedia.org/wiki/Fisher-Yates_shuffle#The_modern_algorithm
 */
void random_permutation(int max_num, int *randperm)
{
  int i,j,temp;

  /* Initialize the array with the results */
  for (i=0; i<max_num; i++)
    randperm[i]=i;

  /* Produce the permutation */
  for (i=max_num-1; i>0; i--)
  {
    j = rand() % i ;
    temp = randperm[j];
    randperm[j] = randperm [i];
    randperm[i] = temp;
  }

}

/*
 * This function returns a pointer to to an n x n adjacency matrix, represented as a 2-d
 * array. This array is produced by "renaming" the nodes of the initial matrix G, using
 * some randomly chosen permutation of [1,n]
 *
 * The function assumes that the caller has already allocated memory for variable E.
 *
 * Note: n is the number of nodes
 */
void permute_nodes(int **G, int nodes, int *randperm, int **E)
{
  int i,j;
  int **E_tmp;

  /* Allocate storage for temporary adjacency matrix */
  E_tmp = malloc(nodes * sizeof(int*));
  for (i=0; i<nodes; i++)
    E_tmp[i] = malloc(nodes * sizeof(int));

  /* TODO:check for a better way to do the exchange */
  /* Do the swap - cols */
  for (i=0; i<nodes; i++ )
      for (j=0; j<nodes; j++ )
        E_tmp[i][randperm[j]] = G[i][j];

  /* Do the swap - rows */
  for (i=0; i<nodes; i++ )
      for (j=0; j<nodes; j++ )
        E[randperm[i]][j] = E_tmp[i][j];

  /* Free variables */
  for (i=0; i<nodes; i++)
    free(E_tmp[i]);
  free(E_tmp);

}

/*
 * Implementation of Floyd's algorithm to select n random integers from a range
 * with no duplicates.
 *
 * The function stores in randarray n values selected randomly from the range
 * [0,max_num]
 *
 * Since Floyd's algorithm chooses values in range 1-MAX_NUM , I have modified the 
 * code in order to get values in range 0-MAX_NUM. 
 *
 * To do that, I compute random values in range 1-(max_num+1) and then I subtract 1.
 *
 * The function assumes that the caller has already allocated memory for variable
 * randarray.
 *
 * References
 * ------------
 * [1] http://dl.acm.org/citation.cfm?id=315746
 *
 */
void unique_random_numbers(int *randarray, int max_num, int n)
{
  int i,j,t;
  int m;
  int *flags;

  m = max_num + 1;

  /* Initialize the flags*/
  flags = malloc( m  * sizeof(int));
  for (i=0; i < m; i++)
    flags[i] = 1;

  i = 0;

  for (j=m-n+1; j <= m; j++)
  {
    t = (rand() % j) + 1 ; /* range 1 - j */
    if (flags[t-1])
    {
      randarray[i] = t-1;
      flags[t-1] = 0;
    }
    else
    {
      randarray[i] = j-1;
      flags[j-1] = 0;
    }
    i++;
  }

  free(flags);
}

/*
 * This function creates the induced subgraph G_ind from graph G using nodes from 
 * nodes_ids. The variable G_ind is the adjacency matrix for the newly created graph
 *
 * The function assumes that the caller has already allocated memory for variable G.
 * 
 * Variable size refers to the size of the induced subgraph (the size of nodes_ids array)
 */
void get_induced_subgraph(int *nodes_ids, int size, int **G, int *edges, int **G_ind)
{
  int i,k;
  int e = 0;

  for (i=0; i<size; i++)
    for (k=i; k<size; k++)
    {
      G_ind[i][k] = G[nodes_ids[i]][nodes_ids[k]];
      G_ind[k][i] = G[nodes_ids[k]][nodes_ids[i]];
      e+=G[nodes_ids[i]][nodes_ids[k]];
    }
  *(edges) = e;
}

/* 
 * This is the comparison function which is used as input to qsort() function
 * for sorting in ascending order
 */
int comparison_function_ascending( const void *a, const void *b)
{
  return *((int*)a) - *((int*) b);
}

/*
 * This is the comparison function which is used as input to qsort() function
 * for sorting in descending order
 */
int comparison_function_descending( const void *a, const void *b)
{
  return *((int*)b) - *((int*) a);
}

/*
 * This function returns a pointer to to an n x n adjacency matrix, represented as a 2-d
 * array. This array is produced by adding some noise to the  matrix G..
 * 
 * Initially, a permutation is applied to the initial graph G and then a delta fraction of
 * nodes is removed randomly. In the remaining graph, an epsilon portion of its edges is
 * removed from random positions and then re-added in random position. The resulting graph
 * is the graph H, which is returned by the function.
 * 
 * According to the implementation, an edge cannot be added in a position from which an
 * edge was removed in the removal phase. Also, checks ensure that an edge cannot be added
 * in order to create a self loop (same node for both ends of the edge).
 *
 * The function allocates memory for variable H, which the caller must free.
 *
 * The caller must also allocate memory for the variables nodesH, edgesH & opt.
 *
 * Variables
 * ---------
 *  G       : the adjacency matrix for the initial graph G
 *  nodesG  : the number of nodes in graph G
 *  edgesG  : the number of edges in graph G
 *  delta   : the fraction of nodes to be removed (must be in range [0,1])
 *  epsilon : the fraction of edges to be removed (must be in range [0,1])
 *  nodesH  : the number of nodes in returning graph H (returned by the function)
 *  edgesH  : the number of edges in returning graph H (returned by the function)
 *  opt     : the correct bijection between the nodes in G and the nodes in H. 
 *            Nodes in G and not in H are noted with the value -1
 *
 * TODO : Put checks for parameter values
 */
int **add_noise(int **G, int nodesG, int edgesG, float delta, float epsilon, int *nodesH, int *edgesH, int *opt)
{

  int **E;
  int **F;
  int **H;
  int n,m;
  int edgesF;
  int *optTmp;
  int *nodes_ids;
  int *nodes_ids_rev;
  int i,j;
  int numrem,numadd;
  int src,dst;

  /* Number of nodes in the input graph G*/
  n = nodesG;

  /* Decide how many nodes will be in the final graph H and in the temporary graph F */
  m = nodesG - (int)nearbyint(floor(delta * nodesG));

  /* Allocate memory for adjacency matrix for temporary graph E */
  E = malloc(n * sizeof(int*));
    for (i=0; i<n; i++)
      E[i] = malloc(n * sizeof(int));

  /* Allocate memory for adjacency matrix for temporary graph F */
  F = malloc(m * sizeof(int*));
    for (i=0; i<m; i++)
      F[i] = malloc(m * sizeof(int));

  /* Allocate memory for adjacency matrix for graph H */
  H = malloc(m * sizeof(int*));
    for (i=0; i<m; i++)
      H[i] = malloc(m * sizeof(int));

  /* Allocate memory for correct bijection optTmp */
  optTmp = malloc(n * sizeof(int));

  /* Permute nodes of G to construct E */
  random_permutation(n,optTmp);
  permute_nodes(G,n,optTmp,E);

  /* Allocate memory for nodes list for graphs F and H */
  nodes_ids = malloc(m * sizeof(int));

  /* Randomly remove nodes from E and construct F  */
  unique_random_numbers(nodes_ids, n-1, m); 
  get_induced_subgraph(nodes_ids, m, E, &edgesF, F);

  /* Randomly remove and add edges from F to construct H */
  /* Copy adjacency matrix from graph F */
  for (i=0; i<m; i++)
    for (j=0; j<m; j++)
      H[i][j] = F[i][j];

  numrem = numadd = (int)nearbyint(floor(epsilon * edgesF )) ;

  /* remove edges */
  while (numrem > 0 )
  {
    src = rand() % m ;
    dst = rand() % m ;

    /* I do the check here using H instead of F in order to 
     * avoid removing the same edge twice */
    
    if (H[src][dst] == 1) 
    {
      H[src][dst] = 0;
      H[dst][src] = 0;
      numrem--;
    }
  }
  /* add edges */
  /* Checking if there was not an edge in F instead of H, we avoid
   * putting back an edge which was removed in the previous step
   */
  while (numadd > 0 )
  {
    src = rand() % m ;
    dst = rand() % m ;
    if ( (src != dst) && (F[src][dst] == 0) )
    {
      H[src][dst] = 1;
      H[dst][src] = 1;
      numadd--;
    }
  }

  /* Fix the vector containing the correct bijection 
   * Use -1 as id in missing nodes */
  /* Step 1 : Create reverse index for nodes_ids*/
  nodes_ids_rev = malloc (n * sizeof(int));
  for (i=0; i<n; i++)
    nodes_ids_rev[i] = -1;
  for (i=0; i<m; i++)
    nodes_ids_rev[nodes_ids[i]] = i;
  /* Step 2 : Use reverse index to compute the correct bijection vector */
  for (i=0; i<n; i++)
    opt[i] = nodes_ids_rev[optTmp[i]];

  /* Free variables */
  for (i=0; i<n; i++)
    free(E[i]);
  free(E);
  for (i=0; i<m; i++)
    free(F[i]);
  free(F);
  free(optTmp);
  free(nodes_ids);
  free(nodes_ids_rev);

  /* Copy values in remaining return pointer variables */
  *nodesH = m;
  *edgesH = edgesF;

  /* Return graph H */
  return(H);
}

/* 
 * This function reads a graph from a given file and creates the adjacency matrix.
 * Apart from the graph, this function also returns the number of  nodes and the number 
 * of edges in the graph.
 *
 * The function assumes that the file containing the graph has the following structure
 *
 * - The first line contains two numbers, the number n of nodes and the number k of 
 *   edges in the graph
 * - Each other line contains two space separated numbers, representing the node ids
 *   for the two ends of an undirected edge.
 *
 * The function allocates memory for variable G which the caller must free.
 *
 */
int **read_random_graph(char *input_fname, int *nodes, int *edges)
{
  int i,j,n,k;
  int e=0;
  FILE *input_fd;
  int **G;

  /* Open file */
  if ((input_fd = fopen(input_fname,"r"))==NULL)
  {
    printf("Can't open file %s\nExiting...\n",input_fname);
    exit(EXIT_FAILURE);
  }
  else
  {
    printf("Opening file %s\n",input_fname);

    /* Read first line (nodes edges) */
    fscanf(input_fd,"%d %d",&n,&k);
  
    /* Allocate memory for adjacency matrix for graph G */
    G = malloc(n * sizeof(int*));
    for (i=0; i<n; i++)
      G[i] = malloc(n * sizeof(int));

    /* Initialize the adjacency matrix */
    for (i=0; i<n; i++)
      for (j=0; j<n; j++)
        G[i][j] = 0;

    /* Fill the adjacency matrix */
    while (fscanf(input_fd,"%d %d",&i,&j)!=EOF)
    {
      G[i][j]=1;
      G[j][i]=1;
      e++;
    }

    /* Do an extra check*/
    if (e!=k)
      printf("WARNING! Mismatch between the declared number of edges and the actual number of edges read from file\n");

    /* Copy values in remaining return pointer variables */
    *(edges)=e;
    *(nodes)=n;

    /* Close file */
    fclose(input_fd);

    printf("Graph read from file %s\n",input_fname);

    /* Return graph G read from file */
    return(G);
  }

}

/* 
 * This function reads a graph from a given file and creates the adjacency matrix.
 * Apart from the graph, this function also returns the number of  nodes and the number 
 * of edges in the graph.
 *
 * The function assumes that the file containing the graph has the following structure
 *
 * - The first line contains two numbers, the number n of nodes and the number k of 
 *   edges in the graph
 * - Each other line contains two space separated numbers, representing the node ids
 *   for the two ends of an undirected edge.
 *
 * The function allocates memory for variable G which the caller must free.
 *
 * We use this function to read graphs from datasets from networkrepository.com
 *
 */
int **read_mm_dataset(char *input_fname, int *nodes, int *edges)
{
  int i,j,n,k;
  int e=0;
  FILE *input_fd;
  int **G;
  char comment[101];

  /* Open file */
  if ((input_fd = fopen(input_fname,"r"))==NULL)
  {
    printf("Can't open file %s\nExiting...\n",input_fname);
    exit(EXIT_FAILURE);
  }
  else
  {
    printf("Opening file %s\n",input_fname);

    /* Read first line (comment) */
    fgets(comment,100,input_fd);

    /* Read second line (rows columns entries -> nodes nodes edges) */
    fscanf(input_fd,"%d %*d %d\n",&n,&k);

    /* Allocate memory for adjacency matrix for graph G */
    G = malloc(n * sizeof(int*));
    for (i=0; i<n; i++)
      G[i] = malloc(n * sizeof(int));

    /* Initialize the adjacency matrix */
    for (i=0; i<n; i++)
      for (j=0; j<n; j++)
        G[i][j] = 0;

    /* Fill the adjacency matrix */
    while (fscanf(input_fd,"%d %d",&i,&j)!=EOF)
    {
      G[i-1][j-1]=1; /* Adjust node ids from 1-based to 0-based*/
      G[j-1][i-1]=1;
      e++;
    }

    /* Do an extra check*/
    if (e!=k)
      printf("WARNING! Mismatch between the declared number of edges and the actual number of edges read from file\n");

    /* Copy values in remaining return pointer variables */
    *(edges)=e;
    *(nodes)=n;

    /* Close file */
    fclose(input_fd);

    printf("Graph read from file %s\n",input_fname);

    /* Return graph G read from file */
    return(G);
  }

}

/* 
 * This function reads a graph from a given file and creates the adjacency matrix.
 * The file belongs to the snap dataset (snap.stanford.edu/data/index.html)
 * This function needs as input the number of nodes in the graph and apart from the
 * graph, it also returns the number of edges in the graph.
 *
 * The number of nodes n needs to be known to the function
 *
 * The function assumes that the caller has already allocated memory for variable G.
 */
void read_snap_file( int n, int *edges, char *input_fname, int **G)
{
  int i,j;
  int e=0;
  FILE *input_fd;

  /* Open file */
  if ((input_fd = fopen(input_fname,"r"))==NULL)
  {
    printf("Can't open file %s\nExiting...\n",input_fname);
    exit(EXIT_FAILURE);
  }
  else
  {
    printf("Opening file %s\n",input_fname);

    /* Initialize the adjacency matrix */
    for (i=0; i<n; i++)
      for (j=0; j<n; j++)
        G[i][j] = 0;

    /* Fill the adjacency matrix */
    while (fscanf(input_fd,"%d %d",&i,&j)!=EOF)
    {
      G[i][j]=1;
      G[j][i]=1;
      e++;
    }
    *(edges)=e;
    printf("Graph read from file %s\n",input_fname);

    /* Close file */
    fclose(input_fd);
  }

}

