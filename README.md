#### README

This repository hosts the implementation used to run the experiments for a publication presented in the IJCAI 2019 conference with title :


"Deanonymizing Social Networks Using Structural Information"


More details about the experiments ran and references to the datasets used can be found in the publication.

Conference site: https://www.ijcai19.org/

Publication link: https://www.ijcai.org/proceedings/2019/169

#### Requirements

This implementation uses the 0.1.3 version of the implementation of Hungarian Algorithm by Cyrill Stachniss, found in the following url:

http://www2.informatik.uni-freiburg.de/~stachnis/misc/libhungarian-v0.1.3.tgz

To build the code, place the files `hungarian.c` and `hungarian.h` (included in the libhungarian-v0.1.3.tgz) inside the `libhungarian` directory.

For building the code, the standard tools for compiling c code will be needed (gcc, ar, make, etc)

__Random network generation__

networkx package is required for the python scripts for random network generation.

#### How to build

To build the code, run `make`.

#### How to run

The makefile produces the following executable files

```
run_experiments_egoFacebook
run_experiments_mmDatasets
run_experiments_random_graphs
test
```

All these can be used to run the algorithms for various input types.

More info for each binary can be found by running with the parameter `-h` . For example:

```
./test -h

```

Some of the binaries are tailored to process the datasets used for the experiments.

#### Datasets used for the experiments

#### References

__Publication links__

https://www.ijcai19.org/

https://www.ijcai.org/proceedings/2019/169

__libhungarian implementation__

http://www2.informatik.uni-freiburg.de/~stachnis/misc/libhungarian-v0.1.3.tgz

http://www2.informatik.uni-freiburg.de/~stachnis/misc.html

https://www.ipb.uni-bonn.de/data-software/
                                                           
