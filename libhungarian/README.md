#### Hungarian algorithm implementation

This implementation of the de-anonymization algorithms uses the following implementation of Hungarian Algorithm by Cyrill Stachniss :

http://www2.informatik.uni-freiburg.de/~stachnis/misc/libhungarian-v0.1.3.tgz

To build the code, uncompress the tgz file and place the following files in this directory before running the Makefile

hungarian.c
hungarian.h

##### References

__libhungarian implementation__

http://www2.informatik.uni-freiburg.de/~stachnis/misc/libhungarian-v0.1.3.tgz

http://www2.informatik.uni-freiburg.de/~stachnis/misc.html 

https://www.ipb.uni-bonn.de/data-software/


