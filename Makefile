CC = gcc
AR = ar

HUNGDIR = ./libhungarian

CFLAGS = -g -O0 -Wall -I. -I$(HUNGDIR)
#CFLAGS = -O3 -Wall -I. -I$(HUNGDIR)
LDFLAGS = -L. -ldeanonymization -lm

DLIB = libdeanonymization.a
VERSION = 0.1

all: $(HUNGLIB) test run_experiments_random_graphs run_experiments_egoFacebook run_experiments_mmDatasets

test: test.c $(DLIB) 
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

run_experiments_random_graphs: run_experiments_random_graphs.c $(DLIB) 
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

run_experiments_egoFacebook: run_experiments_egoFacebook.c $(DLIB) 
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

run_experiments_mmDatasets: run_experiments_mmDatasets.c $(DLIB) 
	$(CC) $(CFLAGS) -o $@ $< $(LDFLAGS)

OBJS = hungarian.o utilities.o algorithms.o

$(DLIB): $(OBJS)
	$(AR) cr $@ $(OBJS)

hungarian.o: $(HUNGDIR)/hungarian.c $(HUNGDIR)/hungarian.h
	$(CC) $(CFLAGS) -c $<

%.o: %.c %.h
	$(CC) $(CFLAGS) -c $<

clean:
	rm -f *.o *.a test run_experiments_random_graphs run_experiments_egoFacebook run_experiments_mmDatasets
