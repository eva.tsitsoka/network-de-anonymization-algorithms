/*
 * C Implementation of De-Anonymization Algorithms
 */

/*
 * a test/example program for the C implementation of De-Anonymization Algorithms
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "hungarian.h"
#include "utilities.h"
#include "algorithms.h"

/* Dataset type codes */
#define RANDOM_GRAPHS 0
#define MM_GRAPHS 1
#define SNAP_GRAPHS 2
#define VALID_MAX 2

void parse_args(int argc, char **argv);
void usage();

// problem parameters - default values
float delta = 0.05;
float epsilon = 0.03;

// input data file
char *input_fname;

// input data type
int dataset_type = -1;
 
int main(int argc, char **argv)
{

  int **G;
  int **H;
  int *opt;
  int n,m;
  time_t t, begin, end;
  int i;
  int edgesG, edgesH;
  int *bijection;
  int *lbijection;

  double cpu_time[4] = {0,0,0,0};
  int correct[4] = {0,0,0,0};
  int correct_common[4] = {0,0,0,0};
  int correct_missing[4] = {0,0,0,0};

  parse_args(argc,argv);


  /* Initialize random generator  */
  srand((unsigned) time(&t));

  printf("\nExecution Started\n\n");

  /* Read Input graph G */
  
  if ( dataset_type == RANDOM_GRAPHS )
    G = read_random_graph(input_fname, &n, &edgesG);
  else if ( dataset_type == MM_GRAPHS )
    G = read_mm_dataset(input_fname, &n, &edgesG);
  else if ( dataset_type == SNAP_GRAPHS )
  {
    n = 4039;
    G = malloc(n * sizeof(int*));
    for (i=0; i<n; i++)
      G[i] = malloc(n * sizeof(int));
    read_snap_file( n, &edgesG, input_fname, G);
  }
  else
    exit(EXIT_FAILURE);

  printf("Read %d nodes and %d edges.\n",n,edgesG);

  opt = malloc(n * sizeof(int));

  /* Create noisy graph H */
  H = add_noise(G, n, edgesG, delta, epsilon, &m, &edgesH, opt);
  printf("Created H with %d nodes and %d edges.\n",m,edgesH);

  /* Run Algorithms */

  printf("\nRun Algorithms\n\n");

  printf("Neighbors Degree Algorithm ... ");
  begin = clock();
  bijection = neighbors_degree_algorithm(G,n,H,m);
  end = clock();
  cpu_time[0] = (double)(end - begin) / CLOCKS_PER_SEC;
  for( i=0; i<n; i++)
  {
    if (opt[i] == bijection[i])
    {
      correct[0]++;
      if (opt[i] == -1)
        correct_missing[0]++;
      else
        correct_common[0]++;
    }
  }

  printf("Finished\n");

  printf("Local Search with Multi-First Fit ... ");
  begin = clock();
  lbijection = local_search_multi_first_fit_algorithm(G, H, bijection, n);
  end = clock();
  cpu_time[1] = (double)(end - begin) / CLOCKS_PER_SEC;
  for( i=0; i<n; i++)
  {
    if (opt[i] == lbijection[i])
    {
      correct[1]++;
      if (opt[i] == -1)
        correct_missing[1]++;
      else
        correct_common[1]++;
    }
  }
  
  printf("Finished\n");

  free(bijection);
  free(lbijection);

  printf("Neighbors Degree Algorithm v2 ... ");
  begin = clock();
  bijection = neighbors_degree_algorithm_v2(G,n,H,m);
  end = clock();
  cpu_time[2] = (double)(end - begin) / CLOCKS_PER_SEC;
  for( i=0; i<n; i++)
  {
    if (opt[i] == bijection[i])
    {
      correct[2]++;
      if (opt[i] == -1)
        correct_missing[2]++;
      else
        correct_common[2]++;
    }
  }
  
  printf("Finished\n");

  printf("Local Search with Multi-First Fit ... ");
  begin = clock();
  lbijection = local_search_multi_first_fit_algorithm(G, H, bijection, n);
  end = clock();
  cpu_time[3] = (double)(end - begin) / CLOCKS_PER_SEC;
  for( i=0; i<n; i++)
  {
    if (opt[i] == lbijection[i])
    {
      correct[3]++;
      if (opt[i] == -1)
        correct_missing[3]++;
      else
        correct_common[3]++;
    }
  }
  
  printf("Finished\n");

  printf("\n\nSummary\n");
  printf("==========\n\n");
  printf("Parameters\n");
  printf("----------\n");
  printf("Dataset Filename : %s\nNoise Parameter Delta : %3.2f\nNoise Parameter Epsilon : %3.2f\n\n",input_fname,delta,epsilon);
  printf("Graph Info\n");
  printf("----------\n");
  printf("Nodes in G : %d\nEdges in G : %d\n",n,edgesG);
  printf("\n");
  printf("Nodes in H : %d\nEdges in H : %d\n\n",m,edgesH);

  for(i=48;i--;printf("="));
  printf(" Results ");
  for(i=48;i--;printf("="));
  printf("\n");
  printf("Algorithm\t\t\tCorrect\t\tCommon\t\tMissing\t\tCPU time (sec)\n");
  for(i=105;i--;printf("="));
  printf("\n");

  printf("Neighbors Degree\t\t%6d\t\t%6d\t\t%6d\t\t%f\n",correct[0],correct_common[0],correct_missing[0],cpu_time[0]);
  printf("Local Search Multi-First Fit\t%6d\t\t%6d\t\t%6d\t\t%f\n",correct[1],correct_common[1],correct_missing[1],cpu_time[1]);
  for(i=105;i--;printf("-"));
  printf("\n");
  printf("Neighbors Degree v2\t\t%6d\t\t%6d\t\t%6d\t\t%f\n",correct[2],correct_common[2],correct_missing[2],cpu_time[2]);
  printf("Local Search Multi-First Fit\t%6d\t\t%6d\t\t%6d\t\t%f\n",correct[3],correct_common[3],correct_missing[3],cpu_time[3]);
  for(i=105;i--;printf("="));
  printf("\n");
  printf("\n");

  /* Free allocated memory*/
  free(opt);
  free(bijection);
  free(lbijection);
  for (i=0; i<n; i++)
    free(G[i]);
  free(G);
  for (i=0; i<m;i++)
    free(H[i]);
  free(H);

  return(0);
}

void parse_args(int argc, char **argv)
{

  int opt;
  int flagt,flagf;
  int fname_size;
  flagt=0;
  flagf=0;

  while ((opt = getopt(argc, argv, "t:f:e:d:h")) != -1)
  {
    switch (opt){
      case 't':
        dataset_type = atoi(optarg);
        if (dataset_type < 0 || dataset_type > VALID_MAX)
        {
          printf("Wrong dataset type. Conslut help about the values for supported datasets\n");
          usage();
          exit(EXIT_FAILURE);
        }
	flagt = 1;
        break;
      case 'f':
        fname_size = strlen(optarg)+1; // input size + 1 for null byte
        input_fname = malloc(fname_size);
        strncpy(input_fname,optarg,fname_size);
        flagf = 1;
        break;
      case 'e':
        epsilon = atof(optarg);
        break;
      case 'd':
        delta = atof(optarg);
        break;
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
      default:
        usage();
        exit(EXIT_FAILURE);
    }
  }
  if (optind < argc) 
  {
    usage();
    exit(EXIT_FAILURE);
  }   

  // Check if all needed args are provided
  if ( !flagf || !flagt )
  {
    usage();
    exit(EXIT_FAILURE);
  }

  if ( delta < 0 || delta > 1 )
  {
    printf("Invalid delta value. Delta must be in range [0,1]\n");
    exit(EXIT_FAILURE);
  }

  if ( epsilon < 0 || epsilon > 1 )
  {
    printf("Invalid epsilon value. Epsilon must be in range [0,1]\n");
    exit(EXIT_FAILURE);
  }

}

void usage()
{
  printf("\nUsage: ./test -t <type> -f <filename> [-d <delta>] [-e <epsilon>]\n");
  printf("Test graph de-anonymization algorithms\n\n"
         "This program takes as input a file describing a graph G and\n"
         "applies some noise to that, creating a new graph H. Next,\n"
         "the program attempts to find the bijection between G and H\n"
         "using the algorithms we developed.\n\n"
         "At the end, the results are presented in a table.\n\n\n"
         "OPTIONS\n========\n"
         "  -t type\tInput Dataset Type\n"
         "  -f filename\tName of the file containing the input graph\n"
         "  -d delta\tDelta noise parameter [default 0.05]\n"
         "  -e epsilon\tEpsilon noise parameter [default 0.03]\n"
         "  -h      \tPrint this help message and exit\n\n\n"
         "EXAMPLES\n---------\n"
         "./test -f watts_strogatz-0.txt -t 0\n"
         "./test -f powerlaw_cluster-9.txt -t 0\n"
         "./test -f socfb-Pepperdine86.mtx -t 1\n"
         "./test -f facebook_combined.txt -t 2 -d 0.00 -e 0.00\n"
         "\n\n"
         "DATASET TYPES\n==============\n"
         "Currently, the following dataset type are supported\n\n"
         "  TYPE\t\t\t    VALUE\tDescription\n"
         "  ----------------------------------------------------------------------------------------------\n"
         "  RANDOM GRAPHS\t\t\t0\tRandom Graphs Created by our custom Python scripts.\n"
         "  MATRIX MARKET GRAPHS\t\t1\tMatrixMarket matrix coordinate pattern symmetric.\n"
         "  \t\t\t\t\tAcquired from networkrepository.com (Facebook Graphs).\n"
         "  SNAP GRAPHS\t\t\t2\tUndirected Graphs from Stanford Network Analysis Project.\n"
         "  \t\t\t\t\tCurenty supported only egoFacebook dataset.\n"
         "\n\n"
         "FILE FORMAT [RANDOM GRAPHS]\n============================\n"
          "The programm expects the first line of the file to be in format <nodes><space><edges>\n"
          "and each other line of the file to be on the form n1<space>n2 where n1,n2 are nodes of\n"
          "the graph that are connected with an edge.\n"
          "Nodes IDs are expected to be zero-based (first ID is 0, second is 1, etc)\n"
         "\n\n"
         "NOISE MODEL\n=============\n"
         "Initially, a permutation is applied to the initial graph G and then a delta fraction of\n"
         "nodes is removed randomly. In the remaining graph, an epsilon portion of its edges is\n"
         "removed from random positions and then re-added in random position. The resulting graph\n"
         "is the graph H.\n\n"
         "According to the implementation, an edge cannot be added in a position from which an\n"
         "edge was removed in the removal phase. Also, checks ensure that an edge cannot be added\n"
         "in order to create a self loop (same node for both ends of the edge).\n"
         "\n\n");
}
