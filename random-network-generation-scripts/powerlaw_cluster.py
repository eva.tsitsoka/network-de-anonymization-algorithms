#!/usr/bin/python

# imports
import networkx as nx
import sys
import getopt

#======================================
# define functions
#======================================

#---- USAGE ---------------------------

def usage():
  print ""
  print "Usage : powerlaw_cluster [options]"
  print ""
  print "DESCRIPTION: The script constructs a number of Holme & Kim graphs."
  print "\t     Each graph is saved in a text file where each line contains an"
  print "\t     edge in the form <node> <node>. The filenames are of the form"
  print "\t     powerlaw_cluster-[num].txt ."
  print ""
  print "OPTIONS\n-----------"
  print "  -n <value> : The number of nodes. [default n=1000]"
  print "  -m <value> : The number of random edges to add for each new node. [default m=30]"
  print "  -p <value> : Probability of adding a triangle after adding a random edge in range [0,1]. [default p=0.10]"
  print ""
  print "  -r <value> : The number of Holme & Kim graphs to create. [default r=10]"
  print ""
  print "  -h : Print this help message and exit."
  print ""

#--------------------------- USAGE ----

#--------------- MAIN -----------------

def main(argv):

  #-------------------------------------
  # Parameters with default values
  #-------------------------------------
  
  # powerlaw_cluster graph parameters
  n = 1000
  p = 0.10
  m = 30
  
  # other parameters
  r = 10
  base_filename = "powerlaw_cluster-"
  extention = ".txt"
  
  #-------------------------------------
  # Read arguments from cli
  #-------------------------------------
  try:
    opts, args = getopt.getopt(argv,"hn:m:p:r:")
  except getopt.GetoptError:
    usage()
    sys.exit(2)

  for opt, arg in opts:
    if opt == '-h':
      usage()  
      sys.exit()
    elif opt == "-n":
      n = int(arg)
    elif opt == "-m":
      m = int(arg)
    elif opt == "-p":
      p = float(arg)
    elif opt == "-r":
      r = int(arg)

  print ""
  print " Holme & Kim Graphs\n======================="
  print ""
  print " Model Parameters\n------------------"
  print "   n : ",n
  print "   m : ",m
  print "   p : ",p
  print ""
  print "Creating",r,"Holme & Kim Graphs..."
  print ""

  #-------------------------------------
  # Graph creation
  #-------------------------------------
  
  for i in range(0,r):
    G = nx.powerlaw_cluster_graph(n,m,p)

    f = open(base_filename+str(i)+extention, 'w')
    # Write graph info in the first line of the file
    f.write(str(n)+" "+str(G.number_of_edges())+"\n")
    # Write graph edgelist
    for line in nx.generate_edgelist(G,data=False):
      f.write(line+"\n")
    f.close()
    #nx.write_edgelist(G,base_filename+str(i)+extention,data=False)
  
  print "Done\n"
#--------------------------------------

#======================================


if __name__ == "__main__":
   main(sys.argv[1:])


