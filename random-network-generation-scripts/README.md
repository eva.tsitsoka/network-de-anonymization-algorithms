#### README

These scripts were used to produce random graphs' snapshots of the correspondent types, used to run experiments for the publication.

More info for each script can be found by running the script with the parameter `-h` . For example:

```
python barabasi_albert.py -h

```

[networkx](https://networkx.org/) package is required for the scripts to run.

Note: The scripts were written for python2 and are not yet ported to python3
