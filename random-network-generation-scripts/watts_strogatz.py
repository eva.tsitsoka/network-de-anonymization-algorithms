#!/usr/bin/python

# imports
import networkx as nx
import sys
import getopt

#======================================
# define functions
#======================================

#---- USAGE ---------------------------

def usage():
  print ""
  print "Usage : watts_strogatz [options]"
  print ""
  print "DESCRIPTION: The script constructs a number of Watts-Strogatz graphs."
  print "\t     Each graph is saved in a text file where each line contains an"
  print "\t     edge in the form <node> <node>. The filenames are of the form"
  print "\t     watts_strogatz-[num].txt ."
  print ""
  print "OPTIONS\n-----------"
  print "  -n <value> : The number of nodes. [default n=1000]"
  print "  -k <value> : Each node is connected to k nearest neighbors in ring topology. [default k=70]"
  print "  -p <value> : The probability of rewring each edge in range [0,1]. [default p=0.10]"
  print ""
  print "  -r <value> : The number of Watts-Strogatz graphs to create. [default r=10]"
  print ""
  print "  -h : Print this help message and exit."
  print ""

#--------------------------- USAGE ----

#--------------- MAIN -----------------

def main(argv):

  #-------------------------------------
  # Parameters with default values
  #-------------------------------------
  
  # watts_strogatz graph parameters
  n = 1000
  p = 0.10
  k = 70
  
  # other parameters
  r = 10
  base_filename = "watts_strogatz-"
  extention = ".txt"
  
  #-------------------------------------
  # Read arguments from cli
  #-------------------------------------
  try:
    opts, args = getopt.getopt(argv,"hn:k:p:r:")
  except getopt.GetoptError:
    usage()
    sys.exit(2)

  for opt, arg in opts:
    if opt == '-h':
      usage()  
      sys.exit()
    elif opt == "-n":
      n = int(arg)
    elif opt == "-k":
      k = int(arg)
    elif opt == "-p":
      p = float(arg)
    elif opt == "-r":
      r = int(arg)

  print ""
  print " Watts-Strogatz Graphs\n======================="
  print ""
  print " Model Parameters\n------------------"
  print "   n : ",n
  print "   k : ",k
  print "   p : ",p
  print ""
  print "Creating",r,"Watts-Strogatz Graphs..."
  print ""

  #-------------------------------------
  # Graph creation
  #-------------------------------------
  
  for i in range(0,r):
    G = nx.watts_strogatz_graph(n,k,p)

    f = open(base_filename+str(i)+extention, 'w')
    # Write graph info in the first line of the file
    f.write(str(n)+" "+str(G.number_of_edges())+"\n")
    # Write graph edgelist
    for line in nx.generate_edgelist(G,data=False):
      f.write(line+"\n")
    f.close()
    #nx.write_edgelist(G,base_filename+str(i)+extention,data=False)
  
  print "Done\n"
#--------------------------------------

#======================================


if __name__ == "__main__":
   main(sys.argv[1:])


