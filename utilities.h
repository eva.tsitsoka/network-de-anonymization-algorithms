#ifndef UTILITIES_H
#define UTILITIES_H

/* Implementation of Durstenfeld version of Fisher-Yates shuffle algorithm
 * Memory for variable randperm must be allocated before 
 * passing it to the function */
void random_permutation(int max_num, int *randperm);

/* Renames the node of G using a random permutation
 * Memory for variable E must be allocated before 
 * passing it to the function */
void permute_nodes(int **G, int nodes, int *randperm, int **E);

/* Implementation of Floyd's algorithm to select n random integers from a range
 * with no duplicates.
 * Memory for variable randarray must be allocated before passing it to the 
 * function
 */
void unique_random_numbers(int *randarray, int max_num, int n);

/* Creates the induced graph G_ind from G by using nodes in nodes_ids
 * Memory for variable G_ind must be allocated before 
 * passing it to the function */
void get_induced_subgraph(int *nodes_ids, int size, int **G, int *edges, int **G_ind);

/* Comparison function to be used as input to qsort() function
 * for sorting in ascending order */
int comparison_function_ascending( const void *a, const void *b);

/* Comparison function to be used as input to qsort() function
 * for sorting in descending order */
int comparison_function_descending( const void *a, const void *b);

/* 
 * Produces graph by adding noise to initial graph G, according to our noise model.
 * Memory for variables nodesH, edgesH & opt must be allocated before calling 
 * the function. 
 * The function allocates memory for return variable, which the caller must free. 
 */
int **add_noise(int **G, int nodesG, int edgesG, float delta, float epsilon, int *nodesH, int *edgesH, int *opt);

/*
 * Reads from file the random graph created by our python scripts, having a specific
 * structure.
 */
int **read_random_graph( char *input_fname, int *nodes, int *edges);

/* Reads graph from file 
 * Memory for variable G must be allocated before 
 * passing it to the function */
int **read_mm_dataset(char *input_fname, int *nodes, int *edges);

/* Reads snap dataset from file 
 * Memory for variable G must be allocated before 
 * passing it to the function */
void read_snap_file( int n, int *edges, char *input_fname, int **G);

#endif
