/*
 * C Implementation of De-Anonymization Algorithms
 */

#include <stdio.h>
#include <stdlib.h>

#include "algorithms.h"
#include "hungarian.h"
#include "utilities.h"


/*
 * This function computes a bijection from graph G to graph H, trying to maximize the 
 * number of edges and non-edges that are preserved after applying the bijection.
 *
 * In case that G and H have different number of nodes, the missing nodes in 
 * bijection are represented with the value -1. The algorithm assumes that
 * nodesG >= nodesH.
 *
 * The algorithm computes a full bipartite graph with the nodes of G and H as independent
 * sets, adding as weight to each edge the difference between its ends. In case H has 
 * less nodes than G, some isolated dummy nodes are added to H so that the resulting 
 * graph Hn to have the same number of nodes with G. 
 * The difference between two nodes is computed using the nodes' neighbors' degree 
 * sequence.
 *
 * The bijection is produced by computing a min cost perfect matching between G & Hn, 
 * using the implementation of the Hungarian Algorithm by Cyrill Stachniss [1].
 *
 * The function assumes that the two graphs are undirected, simple graphs (there
 * are no multiedges or self-loops).
 *
 * The function allocates memory for the array with the results which the caller
 * must free().
 *
 * References
 * ----------
 * [1] http://www2.informatik.uni-freiburg.de/~stachnis/resources.html
 *
 * TODO : Add checks if nodesG > nodesH
 */
int* neighbors_degree_algorithm(int **G, int nodesG, int **H, int nodesH)
{
  int i,j,k;
  int n,m;
  int *bijection;
  int *node_degrees;
  int **Hn;
  int **degree_sequence_G;
  int **degree_sequence_H;
  int **bipartite_graph;
  hungarian_problem_t prob;

  n=nodesG;
  m=nodesH;

  /* Allocate memory for the variables */
  bijection = malloc(n * sizeof(int));
  node_degrees = malloc(n * sizeof(int));
  
  bipartite_graph = malloc(n * sizeof(int *));
  for (i=0; i<n; i++)
    bipartite_graph[i] = malloc(n * sizeof(int));

  degree_sequence_G = malloc(n * sizeof(int *));
  for (i=0; i<n; i++)
    degree_sequence_G[i] = malloc(n * sizeof(int));

  Hn =  malloc(n * sizeof(int *));
  for (i=0;i<n;i++)
    Hn[i] = malloc(n * sizeof(int));

  /* To ease computations, I create degree_sequence_H to be
   * the same size with degree_sequence_G, by filling the
   * vector with zeros at the end. This does not affect the
   * correctness of the computation. */
  degree_sequence_H = malloc(n * sizeof(int *));
  for (i=0; i<n; i++)
    degree_sequence_H[i] = malloc(n * sizeof(int));

  /* Initialize variables */
  for (i=0; i<n; i++)
  {
    bijection[i] = 0;
    node_degrees[i] = 0;
    for (j=0; j<n; j++)
    {
      degree_sequence_G[i][j] = 0;
      degree_sequence_H[i][j] = 0;
      bipartite_graph[i][j] = 0;
    }
  }

  /* Initialize graph Hn */
  for (i=0; i<m; i++)
    for (j=0; j<m; j++)
      Hn[i][j] = H[i][j];
  for (i=m; i<n; i++)
    for (j=0; j<n; j++)
      Hn[i][j] = 0;
  for (i=0; i<m; i++)
    for (j=m; j<n; j++)
      Hn[i][j] = 0;

  /* Compute degree for each node in G */
 for (i=0; i<n; i++)
   for (j=0; j<n; j++)
     node_degrees[i] += G[i][j];

 /* Compute neighbors degree sequence for each node in G */
 for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      degree_sequence_G[i][j] = node_degrees[j] * G[i][j];

  /* Compute degree for each node in Hn */
 for (i=0; i<n; i++)
   node_degrees[i] = 0;
 for (i=0; i<m; i++)
   for (j=0; j<n; j++)
     node_degrees[i] += Hn[i][j];

 /* Compute neighbors degree sequence for each node in Hn */
 for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      degree_sequence_H[i][j] = node_degrees[j] * Hn[i][j];

 /* Create the  weighted full bipartite graph with nodes of G & H and weight in 
  * each edge the difference between the nodes on its ends. */
 for (i=0; i<n; i++)
 {
   qsort((void *)degree_sequence_G[i], n, sizeof(int),comparison_function_descending);
   qsort((void *)degree_sequence_H[i], n, sizeof(int),comparison_function_descending);
 }

  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      for (k=0; k<n; k++)
        bipartite_graph[i][j] += abs( degree_sequence_G[i][k] - degree_sequence_H[j][k]);

  /* Get results from hungarian algorithm */
  hungarian_init(&prob,bipartite_graph,n,n,HUNGARIAN_MODE_MINIMIZE_COST);
  hungarian_solve(&prob);

  /* Put the results in Matching */
  for (i=0; i<n; i++)
    for (j=0; j<n; j++)
      if (prob.assignment[i][j] == 1)
        bijection[i] = j;

  hungarian_free(&prob);

  /* Fix results putting -1 in place of dummy isolated nodes */
  for (i=0; i<n; i++)
    if (bijection[i] >= m)
      bijection[i] = -1;

  
  /* Free variables */
  free(node_degrees);
  for (i=0; i<n; i++)
  {
    free(degree_sequence_G[i]);
    free(degree_sequence_H[i]);
    free(bipartite_graph[i]);
    free(Hn[i]);
  }
  free(degree_sequence_G);
  free(degree_sequence_H);
  free(bipartite_graph);
  free(Hn);

  return(bijection);
}


/*
 * This function computes a bijection from graph G to graph H, trying to maximize the 
 * number of edges and non-edges that are preserved after applying the bijection.
 *
 * In case that G and H have different number of nodes, the missing nodes in 
 * bijection are represented with the value -1. The algorithm assumes that
 * nodesG >= nodesH.
 *
 * The algorithm computes a full bipartite graph with the nodes of G and H as independent
 * sets, adding as weight to each edge the difference between its ends.
 * The difference between two nodes is computed using the nodes' neighbors' degree 
 * sequence.
 *
 * The bijection is produced by computing a min cost perfect matching between G & Hn, 
 * using the implementation of the Hungarian Algorithm by Cyrill Stachniss [1].
 *
 * The function assumes that the two graphs are undirected, simple graphs (there
 * are no multiedges or self-loops).
 *
 * The function allocates memory for the array with the results which the caller
 * must free().
 *
 * References
 * ----------
 * [1] http://www2.informatik.uni-freiburg.de/~stachnis/resources.html
 *
 * TODO : Add checks if nodesG > nodesH
 */
int* neighbors_degree_algorithm_v2(int **G, int nodesG, int **H, int nodesH)
{
  int i,j,k;
  //int nodes,m;
  int *bijection;
  int *node_degrees;
  int **degree_sequence_G;
  int **degree_sequence_H;
  int **bipartite_graph;
  hungarian_problem_t prob;

  int nodes;
  nodes=nodesG;
  //m=nodesH;

  /* Allocate memory for the variables */
  bijection = malloc(nodesG * sizeof(int));
  node_degrees = malloc(nodesG * sizeof(int));
  
  bipartite_graph = malloc(nodesG * sizeof(int *));
  for (i=0; i<nodesG; i++)
    bipartite_graph[i] = malloc(nodesG * sizeof(int));

  degree_sequence_G = malloc(nodes * sizeof(int *));
  for (i=0; i<nodes; i++)
    degree_sequence_G[i] = malloc(nodes * sizeof(int));

  degree_sequence_H = malloc(nodes * sizeof(int *));
  for (i=0; i<nodes; i++)
    degree_sequence_H[i] = malloc(nodes * sizeof(int));

  /* Initialize variables */
  for (i=0; i<nodesG; i++)
  {
    bijection[i] = 0;
    node_degrees[i] = 0;
    for (j=0; j<nodes; j++)
    {
      degree_sequence_G[i][j] = 0;
      degree_sequence_H[i][j] = 0;
      bipartite_graph[i][j] = 0;
    }
  }

  /* Compute degree for each node in G */
 for (i=0; i<nodes; i++)
   for (j=0; j<nodes; j++)
     node_degrees[i] += G[i][j];

 /* Compute neighbors degree sequence for each node in G */
 for (i=0; i<nodes; i++)
    for (j=0; j<nodes; j++)
      degree_sequence_G[i][j] = node_degrees[j] * G[i][j];

  /* Compute degree for each node in H */
 for (i=0; i<nodesH; i++)
 {
   node_degrees[i] = 0;
   for (j=0; j<nodesH; j++)
     node_degrees[i] += H[i][j];
 }

 /* Compute neighbors degree sequence for each node in H */
 for (i=0; i<nodesH; i++)
    for (j=0; j<nodesH; j++)
      degree_sequence_H[i][j] = node_degrees[j] * H[i][j];

 for (i=nodesH; i<nodes; i++)
   for (j=0; j<nodes; j++)
     degree_sequence_H[i][j] = 0;
 for (i=0; i<nodesH; i++)
   for (j=nodesH; j<nodes; j++)
     degree_sequence_H[i][j] = 0;

 /* Create the  weighted full biartite graph with nodes of G & H and weight in 
  * each edge the difference between the nodes on its ends. */
 for (i=0; i<nodes; i++)
 {
   qsort((void *)degree_sequence_G[i], nodes, sizeof(int),comparison_function_descending);
   qsort((void *)degree_sequence_H[i], nodes, sizeof(int),comparison_function_descending);
 }

  for (i=0; i<nodesG; i++)
    for (j=0; j<nodesH; j++)
      for (k=0; k<nodes; k++)
        bipartite_graph[i][j] += abs( degree_sequence_G[i][k] - degree_sequence_H[j][k]);

  /* Get results from hungarian algorithm */
  hungarian_init(&prob,bipartite_graph,nodes,nodes,HUNGARIAN_MODE_MINIMIZE_COST);
  hungarian_solve(&prob);

  /* Initialize bijection vector. Non-matched nodes have the value -1 */
  for (i=0; i<nodes; i++)
    bijection[i] = -1;

  /* Put the results in Matching */
  for (i=0; i<nodesG; i++)
    for (j=0; j<nodesH; j++)
      if (prob.assignment[i][j] == 1)
        bijection[i] = j;

  hungarian_free(&prob);

  
  /* Free variables */
  free(node_degrees);
  for (i=0; i<nodes; i++)
  {
    free(degree_sequence_G[i]);
    free(degree_sequence_H[i]);
    free(bipartite_graph[i]);
  }
  free(degree_sequence_G);
  free(degree_sequence_H);
  free(bipartite_graph);

  return(bijection);
}

/*
 * This function computes the bijection from G to H using some initial matching and local 
 * search technique.
 *
 * The function allocates memory for the array with the results which the caller
 * must free().
 *
 * When the bijection for a node has the value -1, then this node does not appear
 * in graph H.
 *
 * The function allocates memory for the array with the results which the caller
 * must free().
 *
 * Note: 'nodes' is the number of nodes in G as well as the number of nodes in the
 *       bijection.
 */
int* local_search_multi_first_fit_algorithm(int **G, int **H, int *initial_bijection, int nodes)
{
  int *bijection;
  int change;
  int i,k;
  int u1,u2,w;
  int foo;
  int *F;
  int *qual_f;
  int qual_du1;
  int qual_du2;
  
  /* Allocate memory for the results */
  bijection = malloc(nodes * sizeof(int));
  for (i=0; i<nodes; i++)
    bijection[i] = initial_bijection[i];

  /* Allocate memory for the variables */
  qual_f = malloc(nodes * sizeof(int));
  F = malloc(nodes * sizeof(int));
  for (i=0; i<nodes; i++)
    F[i] = bijection[i];

  /* Compute quality for nodes for the initial matching */
  for (i=0; i<nodes; i++) /* iteration on all nodes u of G */
  {   
    qual_f[i]=0;
    if (F[i] != -1)
      for (k=0; k<nodes; k++)
        if (i!=k && F[k] != -1 && G[i][k] == H[F[i]][F[k]])
          qual_f[i]++;
  }   

  /* Do the job */
  do
  {
    change = 0;
    for (u1=0; u1<nodes-1; u1++)
    {
      for (u2=u1+1; u2<nodes; u2++)
      {
        /* Try the following flip */
        foo = F[u2];
        F[u2] = F[u1];
        F[u1] = foo;
        
        /* Compute quality of u1,u2 in the new matching */
        qual_du1 = 0;
        qual_du2 = 0;
        
        /* quality for u1 and u2 */
        if (F[u1] != -1)
          for (w=0; w<nodes; w++)
            if (w!=u1 && F[w] != -1 && G[u1][w] == H[F[u1]][F[w]]) 
              qual_du1++;

        if (F[u2] != -1)
          for (w=0; w<nodes; w++)
            if (w!=u2 && F[w] != -1 && G[u2][w] == H[F[u2]][F[w]])
              qual_du2++;

        if (qual_du1 + qual_du2 > qual_f[u1] + qual_f[u2])
        {
          change = 1;

          /* Update quality of nodes */
          qual_f[u1] = qual_du1;
          qual_f[u2] = qual_du2;
          
          for (w=0; w<nodes; w++) /* iteration on all nodes u of G */
          {   
            if (w!=u1 && w!=u2 && F[w] != -1) // F[w] == bijection[w] because w!=u1 and w!=u2
            {
              if (bijection[u1] == -1)
              {
                  if (G[u1][w]==H[F[u1]][F[w]])
                    qual_f[w]++;
              }
              else if (F[u1] == -1)
              {
                  if (G[u1][w]==H[bijection[u1]][F[w]])
                    qual_f[w]--;
              }
              else
              {
                if (H[bijection[u1]][bijection[w]] != H[F[u1]][F[w]])
                {
                  if (G[u1][w]==H[F[u1]][F[w]])
                    qual_f[w]++;
                  else
                    qual_f[w]--;
                }
              }
              if (bijection[u2] == -1)
              {
                  if (G[u2][w]==H[F[u2]][F[w]])
                    qual_f[w]++;
              }
              else if (F[u2] == -1)
              {
                  if (G[u2][w]==H[bijection[u2]][F[w]])
                    qual_f[w]--;
              }
              else
              {
                if (H[bijection[u2]][bijection[w]] != H[F[u2]][F[w]])
                {
                  if (G[u2][w]==H[F[u2]][F[w]])
                    qual_f[w]++;
                  else
                    qual_f[w]--;
                }
              }
            }
          }
          foo = bijection[u1];
          bijection[u1] = bijection[u2];
          bijection[u2] = foo;
        }           
        else
        {
          foo = F[u2];
          F[u2]=F[u1];
          F[u1]=foo;
        }
      }
    }
  }
  while(change);


  free(qual_f);
  free(F);
  return(bijection);
}

