/*
 * C Implementation of De-Anonymization Algorithms
 */

/*
 * a test/example program for the C implementation of De-Anonymization Algorithms
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <math.h>
#include <time.h>

#include "hungarian.h"
#include "utilities.h"
#include "algorithms.h"

#define LOOPS 10

void parse_args(int argc, char **argv);
void usage();

// problem parameters - default values
float delta = 0.05;
float epsilon = 0.03;
int n;

// input data file
char *input_fname;
char graph_fname[101];

int main(int argc, char **argv)
{

  int **G;
  int **H;
  int *opt;
  int m;
  time_t t, begin, end;
  int i;
  int loopCounter=0;
  int edgesG, edgesH;
  int *bijection;
  int *lbijection;

  double cpu_time[2] = {0,0};
  int correct[2] = {0,0};
  int correct_common[2] = {0,0};
  int correct_missing[2] = {0,0};

  double cpu_time_avg[2] = {0,0};
  double correct_avg[2] = {0,0};
  double correct_common_avg[2] = {0,0};
  double correct_missing_avg[2] = {0,0};

  parse_args(argc,argv);


  /* Initialize random generator  */
  srand((unsigned) time(&t));

  printf("\nExecution Started\n\n");

  /* Allocate memory for graph G */
  G = malloc(n * sizeof(int*));
  for (i=0; i<n; i++)
    G[i] = malloc(n * sizeof(int));
  /* Read Input graph G */
  read_snap_file( n, &edgesG, input_fname, G);

  printf("Read %d nodes and %d edges.\n",n,edgesG);

  for (loopCounter = 0; loopCounter<LOOPS; loopCounter++)
  {  
    printf("\n-------------------\n\n");

    opt = malloc(n * sizeof(int));

    /* Create noisy graph H */
    H = add_noise(G, n, edgesG, delta, epsilon, &m, &edgesH, opt);
    printf("Created H with %d nodes and %d edges.\n",m,edgesH);

    /* Run Algorithms */

    printf("\nRun Algorithms\n\n");

    printf("Neighbors Degree Algorithm ... ");
    begin = clock();
    bijection = neighbors_degree_algorithm(G,n,H,m);
    end = clock();
    cpu_time[0] = (double)(end - begin) / CLOCKS_PER_SEC;
    for( i=0; i<n; i++)
    {
      if (opt[i] == bijection[i])
      {
        correct[0]++;
        if (opt[i] == -1)
          correct_missing[0]++;
        else
          correct_common[0]++;
      }
    }

    printf("Finished\n");

    printf("Local Search with Multi-First Fit ... ");
    begin = clock();
    lbijection = local_search_multi_first_fit_algorithm(G, H, bijection, n);
    end = clock();
    cpu_time[1] = (double)(end - begin) / CLOCKS_PER_SEC;
    for( i=0; i<n; i++)
    {
      if (opt[i] == lbijection[i])
      {
        correct[1]++;
        if (opt[i] == -1)
          correct_missing[1]++;
        else
          correct_common[1]++;
      }
    }
    
    printf("Finished\n");

    /* Keep track to compute the average*/
    for (i=0; i<2; i++)
    {
      cpu_time_avg[i]+=cpu_time[i];
      correct_avg[i]+=correct[i];
      correct_missing_avg[i]+=correct_missing[i];
      correct_common_avg[i]+=correct_common[i];

      /* Re-initialize variables for next run */
      cpu_time[i] = 0;
      correct[i] = 0;
      correct_missing[i] = 0;
      correct_common[i] = 0;
    }

    /* Free allocated memory*/
    free(opt);
    free(bijection);
    free(lbijection);
    for (i=0; i<m;i++)
      free(H[i]);
    free(H);

  }

  /* Compute average values */
  for (i=0; i<2; i++)
  {
    cpu_time_avg[i]=(double)cpu_time_avg[i]/LOOPS;
    correct_avg[i]=(double)correct_avg[i]/LOOPS;
    correct_missing_avg[i]=(double)correct_missing_avg[i]/LOOPS;
    correct_common_avg[i]=(double)correct_common_avg[i]/LOOPS;
  }
  

  /* Print summary */

  printf("\n\nSummary\n");
  printf("==========\n\n");
  printf("Parameters\n");
  printf("----------\n");
  printf("Dataset List File : %s\nNoise Parameter Delta : %3.2f\nNoise Parameter Epsilon : %3.2f\n\n",input_fname,delta,epsilon);
  printf("Graph Info\n");
  printf("----------\n");
  //printf("Nodes in G : %d\nEdges in G : %d\n",n,edgesG);
  printf("Nodes in G : %d\n",n);
  printf("\n");
  //printf("Nodes in H : %d\nEdges in H : %d\n\n",m,edgesH);
  printf("Nodes in H : %d\n\n",m);

  for(i=48;i--;printf("="));
  printf(" Results ");
  for(i=48;i--;printf("="));
  printf("\n");
  printf("Algorithm\t\t\tCorrect\t\tCommon\t\tMissing\t\tCPU time (sec)\n");
  for(i=105;i--;printf("="));
  printf("\n");

  printf("Neighbors Degree\t\t%8.3f\t%8.3f\t%8.3f\t%f\n",correct_avg[0],correct_common_avg[0],correct_missing_avg[0],cpu_time_avg[0]);
  printf("Local Search Multi-First Fit\t%8.3f\t%8.3f\t%8.3f\t%f\n",correct_avg[1],correct_common_avg[1],correct_missing_avg[1],cpu_time_avg[1]);
  for(i=105;i--;printf("="));
  printf("\n");
  printf("\n");

  return(0);
}

void parse_args(int argc, char **argv)
{

  int opt;
  int flagf;
  int flagn;
  int fname_size;
  flagf = 0; 
  flagn = 0;

  while ((opt = getopt(argc, argv, "f:n:e:d:h")) != -1)
  {
    switch (opt){
      case 'f':
        fname_size = strlen(optarg)+1; // input size + 1 for null byte
        input_fname = malloc(fname_size);
        strncpy(input_fname,optarg,fname_size);
        flagf = 1;
        break;
      case 'n':
        n = atoi(optarg);
        flagn = 1;
        break;
      case 'e':
        epsilon = atof(optarg);
        break;
      case 'd':
        delta = atof(optarg);
        break;
      case 'h':
        usage();
        exit(EXIT_SUCCESS);
      default:
        usage();
        exit(EXIT_FAILURE);
    }
  }
  if (optind < argc) 
  {
    usage();
    exit(EXIT_FAILURE);
  }   

  // Check if all needed args are provided
  if ( !flagf || !flagn )
  {
    usage();
    exit(EXIT_FAILURE);
  }

  if ( delta < 0 || delta > 1 )
  {
    printf("Invalid delta value. Delta must be in range [0,1]\n");
    exit(EXIT_FAILURE);
  }

  if ( epsilon < 0 || epsilon > 1 )
  {
    printf("Invalid epsilon value. Epsilon must be in range [0,1]\n");
    exit(EXIT_FAILURE);
  }

}

void usage()
{
  printf("\nUsage: ./run_experiments_egoFacebook -f <filename> -n <nodes> [-d <delta>] [-e <epsilon>]\n");
  printf("Test graph de-anonymization algorithms\n\n"
         "This program takes as input a file describing a graph G and\n"
         "applies some noise to that, creating a new graph H. Next,\n"
         "the program attempts to find the bijection between G and H\n"
         "using the algorithms we developed.\n\n"
         "The program repeats the procedure of creating the noisy graph H and\n"
         "running the algorithms %d times.\n\n"
         "At the end, the results (average values) are presented in a table.\n\n\n"
         "OPTIONS\n========\n"
         "  -f filename\tName of the file containing the graph\n"
         "  -n nodes\tNumber of nodes in G\n"
         "  -d delta\tDelta noise parameter [default 0.05]\n"
         "  -e epsilon\tEpsilon noise parameter [default 0.03]\n"
         "  -h      \tPrint this help message and exit\n\n\n"
         "EXAMPLES\n---------\n"
         "./run_experiments_egoFacebook -f facebook_combined_4039.txt -n 4039 -d 0.00 -e 0.00\n"
         "\n\n"
         "GRAPH FILE FORMAT [SNAP - egoFacebook]\n=======================================\n"
         "The programm expects each line of the file to be on the form n1<space>n2\n"
         "where n1,n2 are nodes of the graph that are connected with an edge.\n"
         "Nodes IDs are expected to be zero-based (first ID is 0, second is 1, etc)\n"
         "\n\n"
         "NOISE MODEL\n=============\n"
         "Initially, a permutation is applied to the initial graph G and then a delta fraction of\n"
         "nodes is removed randomly. In the remaining graph, an epsilon portion of its edges is\n"
         "removed from random positions and then re-added in random position. The resulting graph\n"
         "is the graph H.\n\n"
         "According to the implementation, an edge cannot be added in a position from which an\n"
         "edge was removed in the removal phase. Also, checks ensure that an edge cannot be added\n"
         "in order to create a self loop (same node for both ends of the edge).\n"
         "\n\n",LOOPS);
}
