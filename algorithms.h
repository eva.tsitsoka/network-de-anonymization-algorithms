#ifndef ALGORITHMS_H
#define ALGORITHMS_H

/* Computes and returns a bijection from G to H.
 * Allocates memory which the caller must free() */
int* neighbors_degree_algorithm(int **G, int nodesG, int **H, int nodesH);

/* Computes and returns a bijection from G to H.
 * Allocates memory which the caller must free() */
int* neighbors_degree_algorithm_v2(int **G, int nodesG, int **H, int nodesH);

/* Computes and returns a bijection from G to H using an initial bijection
 * and local search techniques.
 * Allocates memory which the caller must free() */
int* local_search_multi_first_fit_algorithm(int **G, int **H, int *initial_bijection, int nodes);

#endif
